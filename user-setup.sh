#!/bin/bash

# Check internet connection and exit
# script if you're offline.
wget -q --tries=10 --timeout=20 --spider http://google.com
if [[ !$? -eq 0 ]]; then
  ip link set group default down
	wifi-menu -o
fi

# Variables used for formatting console output.
bold=$(tput bold)
normal=$(tput sgr0)

# Create tmp folder to download packages.
mkdir -p ~/tmp/pacaur_install
cd ~/tmp/pacaur_install

# Install "cower" from AUR.
if [ ! -n "$(pacman -Qs cower)" ]; then
  curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=cower
  makepkg PKGBUILD --skippgpcheck --install --needed
fi

# Install "pacaur" from AUR.
if [ ! -n "$(pacman -Qs pacaur)" ]; then
  curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=pacaur
  makepkg PKGBUILD --install --needed
fi

# Change shell to zsh.
chsh -s /bin/zsh

# Clean up.
cd ~/tmp
rm -r ~/tmp/pacaur_install

# Install packages
cat ~/packages.list | sed 's/# aur//' | xargs pacaur -S --noconfirm

# Set default X11 keymap
echo -e 'setxkbmap us\nexec i3' > ~/.xinitrc

# Clone arch-setup repo into projects folder
git clone https://eblakem@bitbucket.org/eblakem/arch-setup.git ~/projects/arch-setup
cd ~/projects

# Link dots to config folder
~/projects/arch-setup/link.sh

# Remove .bash_profile so setup isn't executed again.
rm ~/.bash_profile

# Create .zprofile so graphical interface is 
# started automatically on login.
echo "startx" > ~/.zprofile

# Touchpad support
cp /usr/share/X11/xorg.conf.d/70-synaptics.conf /etc/X11/xorg.conf.d/
