#!/bin/bash

# Check internet connection and exit
# script if you're offline.
wget -q --tries=10 --timeout=20 --spider http://google.com
if [[ !$? -eq 0 ]]; then
	ip link set group default down
	wifi-menu -o
fi

# Variables used for formatting console output.
bold=$(tput bold)
normal=$(tput sgr0)

# Sudo user
# read -p "${bold}Usename: ${normal}" username
username="michael"

# Install sudo, create new user, add it to sudoers, and set password.
pacman -S --noconfirm sudo
useradd -m -G wheel $username
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
passwd $username

# Customize pacman
sed -i -e 's/#Color/Color/g' /etc/pacman.conf
sed -i -e 's/#TotalDownload/TotalDownload/g' /etc/pacman.conf

# Install zsh and set it as default shell.
pacman -S --noconfirm zsh
chsh -s /bin/zsh

# Install some common packages and pacaur dependencies.
pacman -S --noconfirm wget tar git unzip vim base-devel
pacman -S --noconfirm --needed expac yajl

# Remove .bash_profile, so the configuration script is not
# executed again.
rm ~/.bash_profile

# Add the configuration script for the user on its .bash_profile.
cp ~/user-setup.sh /home/$username
cp ~/packages.list /home/$username
chown $username /home/$username/user-setup.sh
chown $username /home/$username/packages.list
chown $username /home/$username/.bash_profile
echo "exec ~/user-setup.sh" > /home/$username/.bash_profile

# Check for updates
pacman -Syu --noconfirm

