#!/bin/bash

# Download scripts from my repo..
wget https://bitbucket.org/eblakem/arch-setup/raw/HEAD/install.sh
wget https://bitbucket.org/eblakem/arch-setup/raw/HEAD/install-base.sh
wget https://bitbucket.org/eblakem/arch-setup/raw/HEAD/root-setup.sh
wget https://bitbucket.org/eblakem/arch-setup/raw/HEAD/user-setup.sh
wget https://bitbucket.org/eblakem/arch-setup/raw/HEAD/packages.list


# ..make them executable..
chmod +x *.sh

# ..and run the main script.
./install.sh
